# Chat Server

A simple chat server written in Golang, with basic features:
  * There is many chat rooms
  * User can connect to the chat server
  * User can set their name
  * User can create and join rooms - only one at a time
  * User can see previous messages that have been sent in the room before he joined
  * User can send message to the room
  * User can send private messages to other users - no need to be in specific room

## Chat commands

  * Type `/create roomName` to create a room - only if there is no room with such name
  * Type `/join roomName` to join an existing room
  * Type `/w name message` to send private message (whishper) to someone who is connected to the server
  * After you join a room typeing normali will send the message

## Protocol

I use simple text-based message over TCP:

  * All messages are terminated with `\n`
  * To create a chat room, client will send: 
    * `CREATE roomName`
  * To join a chat room, client will send: 
    * `JOIN roomName`
  * To send a chat message, client will send: 
    * `SEND message`
  * To send a chat message, client will send: 
    * `WHISPER username message`
  * To set client name, client will send:
    * `NAME username`
    * For now, username can not contain space
  * Server will send the following command to all clients in the room when there are new message:
    * `MESSAGE username the actual message` if normal room message
    * `WHISPER username message` when its private
  * Server will send the following command to user when there is a problem:
    * `ERROR message`

### Installation
 * You will need Go, MySQL, TUI
 * To start the server localy go to - `components\server\cmd` and run `go run main.go`
   * on default its listening on `:3333` port
   * you need to configurate the DB addres too - hi-hi - sorry for that - in `components\server\tcp_server.go` - 39 line
 * To start the client go to `components\tui\cmd` and type `go run main.go -server :3333` where you can chose at with port you want ot connect / addres if you have where to host server and DB
 * If there is anything else - you can ask me :)

## References
  * https://github.com/nqbao/go-sandbox/tree/chat/0.0.1/chatserver
  * [tui-go](https://github.com/marcusolsson/tui-go) for chat client.
