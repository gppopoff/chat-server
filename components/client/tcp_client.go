package client

import (
	"chat-server/components/protocol"
	"io"
	"log"
	"net"
)

type TcpChatClient struct {
	conn      net.Conn
	cmdReader *protocol.CommandReader
	cmdWriter *protocol.CommandWriter
	name      string
	incoming  chan protocol.MessageCommand
}

// NewClient constructs new TcpClient
func NewClient() *TcpChatClient {
	return &TcpChatClient{
		incoming: make(chan protocol.MessageCommand),
	}
}

// Dial connect client to a server on a given address
func (c *TcpChatClient) Dial(address string) error {
	conn, err := net.Dial("tcp", address)

	if err == nil {
		c.conn = conn
	}

	c.cmdReader = protocol.NewCommandReader(conn)
	c.cmdWriter = protocol.NewCommandWriter(conn)

	return err
}

// Start read commands from the reader and convert them to MessageCommand in put them in incoming channel from where they can be written on the screen
func (c *TcpChatClient) Start() {
	for {
		cmd, err := c.cmdReader.Read()

		if err == io.EOF {
			break
		} else if err != nil {
			log.Printf("Read error %v", err)
			break
		}

		if cmd != nil {
			switch v := cmd.(type) {
			case protocol.MessageCommand:
				c.incoming <- v
			case protocol.WhisperCommand:
				name := "[(" + v.Name + ")"
				message := v.Message + "]"
				c.incoming <- protocol.MessageCommand{
					Name:    name,
					Message: message,
				}
			case protocol.ErrorCommand:
				err := v.Error + "!"
				c.incoming <- protocol.MessageCommand{
					Name:    "!--Error--",
					Message: err,
				}

			default:
				log.Printf("Unknown command: %v", v)
			}
		}
	}
}

// Close closes the client connection
func (c *TcpChatClient) Close() {
	c.conn.Close()
}

// Incoming returns incoming channel
func (c *TcpChatClient) Incoming() chan protocol.MessageCommand {
	return c.incoming
}

// Send sends command to the server using cmdWriter
func (c *TcpChatClient) Send(command interface{}) error {
	return c.cmdWriter.Write(command)
}

// SetName send NameCommand with the given name
func (c *TcpChatClient) SetName(name string) error {
	return c.Send(protocol.NameCommand{
		Name: name,
	})
}

// SendMessage send SendCommand with the given message
func (c *TcpChatClient) SendMessage(message string) error {
	return c.Send(protocol.SendCommand{
		Message: message,
	})
}

// CreateRoom send CreateCommand with the given roomName
func (c *TcpChatClient) CreateRoom(roomName string) error {
	return c.Send(protocol.CreateCommand{
		RoomName: roomName,
	})
}

// JoinRoom send Join command with the given roomName
func (c *TcpChatClient) JoinRoom(roomName string) error {
	return c.Send(protocol.JoinCommand{
		RoomName: roomName,
	})
}

// WhisperMessage send Whisper command with the given name and message
func (c *TcpChatClient) WhisperMessage(name, message string) error {
	return c.Send(protocol.WhisperCommand{
		Name:    name,
		Message: message,
	})
}
