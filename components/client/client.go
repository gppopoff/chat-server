// Package client contains the client interface and its tcp implementation with we use
package client

import (
	"chat-server/components/protocol"
)

type ChatClient interface {
	Dial(address string) error
	Start()
	Close()
	Send(command interface{}) error
	SetName(name string) error
	SendMessage(message string) error
	CreateRoom(roomName string) error
	JoinRoom(roomName string) error
	WhisperMessage(name, message string) error
	Incoming() chan protocol.MessageCommand
}
