package protocol

import (
	"bytes"
	//	"components/protocol"
	"testing"
)

// var tests = []stuct {
// 	//commands []interface{}
// 	result string
// }{
func TestWriteCommand(t *testing.T) {
	var tests = []struct {
		commands []interface{}
		result   string
	}{
		{
			[]interface{}{
				SendCommand{"Hello"},
			},
			"SEND Hello\n",
		},
		{
			[]interface{}{
				ErrorCommand{"Problem"},
			},
			"ERROR Problem\n",
		},
		{
			[]interface{}{
				MessageCommand{"gosho", "Kak sme"},
			},
			"MESSAGE gosho Kak sme\n",
		},
		{
			[]interface{}{
				WhisperCommand{"pesho", "Kak sme"},
			},
			"WHISPER pesho Kak sme\n",
		},
		{
			[]interface{}{
				NameCommand{"Gosho"},
			},
			"NAME Gosho\n",
		},
		{
			[]interface{}{
				CreateCommand{"Staq"},
			},
			"CREATE Staq\n",
		},
		{
			[]interface{}{
				JoinCommand{"Chushka"},
			},
			"JOIN Chushka\n",
		},
	}

	//var buf *bytes.Buffer
	buf := new(bytes.Buffer)
	for _, test := range tests {
		buf.Reset()
		cmdWriter := NewCommandWriter(buf)

		for _, cmd := range test.commands {
			if cmdWriter.Write(cmd) != nil {
				t.Errorf("Unable to write commands %v", cmd)
			}
		}

		if buf.String() != test.result {
			t.Errorf("Commands output in not the same: %v %v", buf.String(), test.result)
		}
	}
}
