// Package protocol provides commands that will be used for communicating and reader/writer for them
package protocol

import "errors"

var UnknownCommand = errors.New("Unknown command")

// SendCommand is used when user is sending new message form client
type SendCommand struct {
	Message string
}

// MessageCommand is used when sever sends clients new messages
type MessageCommand struct {
	Name    string
	Message string
	// Time string ??
}

// NameCommand / or later LoginCommand - is used for setting client user
type NameCommand struct {
	Name string
	//Password string - for later when there is DB
}

// CreateRoom is used to create new chat room
type CreateCommand struct {
	RoomName string
}

// JoinRoom is used to join certan char room
type JoinCommand struct {
	RoomName string
}

// WhisperCommand is used to send a private message to someone
type WhisperCommand struct {
	Name    string
	Message string
}

type ErrorCommand struct {
	Error string
}

//TODO: add ExitCommand for exiting room
//!!!! also whishper command - for communicating between seperate clients
