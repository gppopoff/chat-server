package protocol

import (
	"bufio"
	"io"
	"log"
)

type CommandReader struct {
	reader *bufio.Reader
}

func NewCommandReader(reader io.Reader) *CommandReader {
	return &CommandReader{
		reader: bufio.NewReader(reader),
	}
}

// Read parses strings to Commands
func (r *CommandReader) Read() (interface{}, error) {
	commandName, err := r.reader.ReadString(' ')

	if err != nil {
		return nil, err
	}

	//TODO: Exit command
	switch commandName {
	case "SEND ":
		message, err := r.reader.ReadString('\n')

		if err != nil {
			return nil, err
		}

		return SendCommand{
			message[:len(message)-1]}, nil

	case "MESSAGE ":
		name, err := r.reader.ReadString(' ')

		if err != nil {
			return nil, err
		}

		message, err := r.reader.ReadString('\n')

		if err != nil {
			return nil, err
		}

		return MessageCommand{
			name[:len(name)-1],
			message[:len(message)-1]}, nil
		//time[:len(time)-1]
		//message[1:len(message)-1]
	case "WHISPER ":
		name, err := r.reader.ReadString(' ')

		if err != nil {
			return nil, err
		}

		message, err := r.reader.ReadString('\n')

		if err != nil {
			return nil, err
		}

		return WhisperCommand{
			name[:len(name)-1],
			message[:len(message)-1]}, nil
		//time[:len(time)-1]
		//message[1:len(message)-1]

	case "NAME ":
		name, err := r.reader.ReadString('\n')

		if err != nil {
			return nil, err
		}

		return NameCommand{
			name[:len(name)-1]}, nil

	case "CREATE ":
		roomName, err := r.reader.ReadString('\n')

		if err != nil {
			return nil, err
		}

		return CreateCommand{
			roomName[:len(roomName)-1]}, nil

	case "JOIN ":
		roomName, err := r.reader.ReadString('\n')

		if err != nil {
			return nil, err
		}

		return JoinCommand{
			roomName[:len(roomName)-1]}, nil

	case "ERROR ":
		error, err := r.reader.ReadString('\n')

		if err != nil {
			return nil, err
		}

		return ErrorCommand{
			error[:len(error)-1]}, nil

	default:
		log.Printf("Unknown command: %v", commandName)
	}

	return nil, UnknownCommand
}

func (r *CommandReader) ReadAll() ([]interface{}, error) {
	commands := []interface{}{}

	for {
		command, err := r.Read()

		if command != nil {
			commands = append(commands, command)
		}

		if err == io.EOF {
			break
		} else if err != nil {
			return commands, err
		}
	}

	return commands, nil
}
