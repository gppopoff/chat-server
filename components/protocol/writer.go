package protocol

import (
	"fmt"
	"io"
)

type CommandWriter struct {
	writer io.Writer
}

// Constructor for CommandWriter
func NewCommandWriter(writer io.Writer) *CommandWriter {
	return &CommandWriter{
		writer: writer,
	}
}

//
func (w *CommandWriter) writeString(msg string) error {
	_, err := w.writer.Write([]byte(msg))

	return err
}

// Pars commands into string so they can be send to the server
// In case of new commands need to add cases to the swich
func (w *CommandWriter) Write(command interface{}) error {
	var err error

	//TODO: Exit command
	switch v := command.(type) {
	case SendCommand:
		err = w.writeString(fmt.Sprintf("SEND %v\n", v.Message))

	// TO-DO: Think about time - how to add it to the message
	// err = w.writeString(fmt.Sprintf("MESSAGE %v [%v] %v\n", v.Name, v.Time, v.Message))
	case MessageCommand:
		err = w.writeString(fmt.Sprintf("MESSAGE %v %v\n", v.Name, v.Message))
	case WhisperCommand:
		err = w.writeString(fmt.Sprintf("WHISPER %v %v\n", v.Name, v.Message))

	// TO-DO: Change when DB is implemented and it is Login Command
	case NameCommand:
		err = w.writeString(fmt.Sprintf("NAME %v\n", v.Name))

	case CreateCommand:
		err = w.writeString(fmt.Sprintf("CREATE %v\n", v.RoomName))

	case JoinCommand:
		err = w.writeString(fmt.Sprintf("JOIN %v\n", v.RoomName))

	case ErrorCommand:
		err = w.writeString(fmt.Sprintf("ERROR %v\n", v.Error))

	default:
		err = UnknownCommand
	}

	return err
}
