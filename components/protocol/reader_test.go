package protocol_test

import (
	"chat-server/components/protocol"
	"reflect"
	"strings"
	"testing"
)

func TestCommandReader(t *testing.T) {
	tests := []struct {
		input   string
		results []interface{}
	}{
		{
			"SEND test\n",
			[]interface{}{
				protocol.SendCommand{"test"},
			},
		},
		{
			"ERROR error\n",
			[]interface{}{
				protocol.ErrorCommand{"error"},
			},
		},
		{
			"MESSAGE Gosho Kak sme brat miii\n",
			[]interface{}{
				protocol.MessageCommand{"Gosho", "Kak sme brat miii"},
			},
		},
		{
			"WHISPER Gosho Kak sme brat miii\n",
			[]interface{}{
				protocol.WhisperCommand{"Gosho", "Kak sme brat miii"},
			},
		},
		{
			"NAME Pesho\n",
			[]interface{}{
				protocol.NameCommand{"Pesho"},
			},
		},
		{
			"CREATE Supa\n",
			[]interface{}{
				protocol.CreateCommand{"Supa"},
			},
		},
		{
			"JOIN musaka\n",
			[]interface{}{
				protocol.JoinCommand{"musaka"},
			},
		},
		{
			"MESSAGE user1 hello\nMESSAGE user2 world\n",
			[]interface{}{
				protocol.MessageCommand{"user1", "hello"},
				protocol.MessageCommand{"user2", "world"},
			},
		},
		{
			"CREATE Supa\nJOIN Supa\nMESSAGE Gosho mn e zle\n",
			[]interface{}{
				protocol.CreateCommand{"Supa"},
				protocol.JoinCommand{"Supa"},
				protocol.MessageCommand{"Gosho", "mn e zle"},
			},
		},
		{
			"CREATE Supa\nJOIN Supa\nNAME Gosho\nMESSAGE Gosho mn e zle\n",
			[]interface{}{
				protocol.CreateCommand{"Supa"},
				protocol.JoinCommand{"Supa"},
				protocol.NameCommand{"Gosho"},
				protocol.MessageCommand{"Gosho", "mn e zle"},
			},
		},
		{
			"CREATE Supa\nJOIN Supa\nNAME Gosho\nWHISPER Gosho mn e zle\nMESSAGE Gosho mn e zle\n",
			[]interface{}{
				protocol.CreateCommand{"Supa"},
				protocol.JoinCommand{"Supa"},
				protocol.NameCommand{"Gosho"},
				protocol.WhisperCommand{"Gosho", "mn e zle"},
				protocol.MessageCommand{"Gosho", "mn e zle"},
			},
		},
	}

	for _, test := range tests {
		reader := protocol.NewCommandReader(strings.NewReader(test.input))
		results, err := reader.ReadAll()

		t.Log(results)

		if err != nil {
			t.Errorf("Unable to read command, error %v", err)
		} else if !reflect.DeepEqual(results, test.results) {
			t.Errorf("Command output is not the same: %v %v", results, test.results)
		}
	}
}
