package server

import (

	//"chat-server/components/server"
	"chat-server/components/protocol"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"io"
	"log"
	"net"
	"sync"
)

type client struct {
	conn   net.Conn
	name   string
	writer *protocol.CommandWriter
	roomId int
}

type room struct {
	clients []*client
	name    string
}

type TcpChatServer struct {
	listener  net.Listener
	rooms     []*room
	pendings  []*client
	mutex     *sync.Mutex
	roomMutex *sync.Mutex
	db        *sql.DB
}

// NewServer - Constructor
func NewServer() *TcpChatServer {
	db, err := sql.Open("mysql", "root:parola123@tcp(127.0.0.1:3306)/chat_db_schema")

	if err != nil {
		panic(err)
	}

	return &TcpChatServer{
		mutex:     &sync.Mutex{},
		roomMutex: &sync.Mutex{},
		db:        db,
	}
}

// Listen make server to start listen on address
func (s *TcpChatServer) Listen(address string) error {
	l, err := net.Listen("tcp", address)

	if err != nil {
		return err
	}

	s.listener = l
	log.Printf("Listening on %v", address)

	return err

}

// Close make server stop listening at the address by closing the listener
func (s *TcpChatServer) Close() {
	s.listener.Close()
}

// Start creates a loot in which server accepts new clients and starts new goroutine that serves them
func (s *TcpChatServer) Start() {
	for {
		conn, err := s.listener.Accept()

		if err != nil {
			log.Printf("Accured error with accepting: %v", err)
		}

		client := s.accept(conn)
		go s.serve(client)
	}
}

// accept add new client to the List of clients
func (s *TcpChatServer) accept(conn net.Conn) *client {
	log.Printf("Accepting connection from %v, total pending clients: %v", conn.RemoteAddr().String(), len(s.pendings)+1)

	s.mutex.Lock()
	defer s.mutex.Unlock()

	client := &client{
		conn:   conn,
		writer: protocol.NewCommandWriter(conn),
	}

	s.pendings = append(s.pendings, client)

	return client
}

// isClientInAnyRoom return the room and its index, in which client is, otherwise nil
// With the database working this function is not needed
func (s *TcpChatServer) isClientInAnyRoom(client *client) (*room, int) {
	for i, room := range s.rooms {
		for _, suitor := range room.clients {
			if suitor == client {
				return room, i
			}
		}
	}

	return nil, 0
}

// serve serve commands that client send to the server
func (s *TcpChatServer) serve(client *client) {
	cmdReader := protocol.NewCommandReader(client.conn)

	defer s.remove(client)

	for {
		cmd, err := cmdReader.Read()

		if err != nil && err != io.EOF {
			log.Printf("Read error: %v", err)
			break
		}

		if cmd != nil {
			switch v := cmd.(type) {
			case protocol.NameCommand:
				client.name = v.Name
			case protocol.SendCommand:

				// IF client have roomId we start goroutine that will send his msg to all users with samo roomId
				if client.roomId != 0 {
					go func(roomId int, sender, msg string) {
						for _, client := range s.pendings {
							if client.roomId == roomId {
								client.writer.Write(protocol.MessageCommand{
									Name:    sender,
									Message: msg,
								})
							}
						}
					}(client.roomId, client.name, v.Message)

					// Here we enter the new message in the databese
					_, err = s.db.Exec("INSERT INTO message (chat_room_id, `user`, content) VALUES (?, ?, ?)", client.roomId, client.name, v.Message)
					if err != nil {
						log.Println(err)
					}

					/*room, _ := s.isClientInAnyRoom(client)
					//go func() {
					//	err := s.Broadcast(protocol.MessageCommand{
					//		Message: v.Message,
					//		Name:    client.name,
					//	}, room.name)
					//
					//	if err != nil {
					//		log.Printf("An Error occurred while Broadcasting: %v", err)
					//	}
					//}()*/

				} else {
					client.writer.Write(protocol.ErrorCommand{Error: "you are not in a room"})
					log.Printf("Client %v is not in a room to send messages", client.conn.RemoteAddr().String())
				}
			case protocol.WhisperCommand:

				err := s.Whisper(protocol.WhisperCommand{
					Name:    client.name,
					Message: v.Message,
				}, v.Name)

				if err != nil {
					client.writer.Write(protocol.ErrorCommand{Error: "message was not send"})
				}

			case protocol.JoinCommand:

				/*//s.roomMutex.Lock()
				//var foundRoom bool
				//
				//for _, room := range s.rooms {
				//	if v.RoomName == room.name {
				//		room.clients = append(room.clients, client)
				//		foundRoom = true
				//
				//		//TOREMOVE
				//		fmt.Println("Izlizame ot vlizaneto v staq")
				//
				//		break
				//	}
				//
				//}
				//s.roomMutex.Unlock()*/

				var roomId int

				err := s.db.QueryRow("SELECT id FROM chat_room WHERE `name` = ?", v.RoomName).Scan(&roomId)

				if err != nil {
					log.Println(err)
					client.writer.Write(protocol.ErrorCommand{Error: "no such room"})
				} else {
					client.roomId = roomId
					row, err := s.db.Query("SELECT `user`, content FROM message WHERE chat_room_id =?", roomId)
					if err != nil {
						log.Println(err)
					}

					var userName, msg string
					for row.Next() {
						err = row.Scan(&userName, &msg)
						if err != nil {
							log.Println(err)
						}
						client.writer.Write(protocol.MessageCommand{
							Name:    userName,
							Message: msg,
						})
					}

				}

			case protocol.CreateCommand: // if its CreateCommand - we send INSERT and if it fails - then such room should exist already

				/*s.roomMutex.Lock()
				//defer s.roomMutex.Unlock()
				var roomAlreadyExist bool
				for _, checkRoom := range s.rooms {
					if checkRoom.name == v.RoomName {
						roomAlreadyExist = true
					}
				}

				if !roomAlreadyExist {
					room := &room{
						name: v.RoomName,
					}
					s.rooms = append(s.rooms, room)

					_, err = s.db.Exec("INSERT INTO chat_room (name) VALUES (?)", v.RoomName)

					if err != nil {
						log.Println(err)
					}

					log.Printf("")
				} else {
					client.writer.Write(protocol.ErrorCommand{Error: "room already exist"})
					log.Println("Room with that name already exist")
				}
				s.roomMutex.Unlock()*/

				_, err = s.db.Exec("INSERT INTO chat_room (name) VALUES (?)", v.RoomName)

				if err != nil {
					log.Println(err)
					client.writer.Write(protocol.ErrorCommand{Error: "room already exist"})
					log.Println("Room with that name already exist")
				}

			default:
				log.Printf("Unknown command error: %v", cmd)
			}
		}
		if err == io.EOF {
			break
		}
	}
}

// remove remove client when his connection is over
func (s *TcpChatServer) remove(clientToRemove *client) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	//remove client from pendings/clientsQueue and from the rooms he had joined
	for i, client := range s.pendings {
		if client == clientToRemove {
			s.pendings = append(s.pendings[:i], s.pendings[i+1:]...)
		}
	}

	/*room, i := s.isClientInAnyRoom(clientToRemove)

	if room != nil {
		room.clients = append(room.clients[:i], room.clients[i+1:]...)
	} else {
		log.Printf("Clinet was not in a single room!")
	}*/

	log.Printf("Closing connection from %v", clientToRemove.conn.RemoteAddr().String())
	clientToRemove.conn.Close()
}

// Broadcast send the message to everyone in the room with that roomName
// With database we send messages directly and this function is not needed - but its Legacy
func (s *TcpChatServer) Broadcast(command interface{}, roomName string) error {
	for _, room := range s.rooms {
		if room.name == roomName {
			for _, client := range room.clients {
				err := client.writer.Write(command)
				// TODO handle errors !? - maybe try to resend
				if err != nil {
					log.Printf("Error accured while writing to %v, %v", client.conn.RemoteAddr().String(), err)
				}
			}
		}
	}
	return nil // for now
}

// Whisper finds the person to which message is whispered, writes to them the MessageCommand and returns error if client with such name do not exist
func (s *TcpChatServer) Whisper(command interface{}, toUser string) error {
	var found bool
	for _, user := range s.pendings {
		if user.name == toUser {
			err := user.writer.Write(command)
			if err != nil {
				return err
			}
			found = true
		}
	}

	if !found {
		return fmt.Errorf("no such user found")
	}
	return nil
}
