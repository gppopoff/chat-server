// Package server contains the chat server interface and tcp implementation
package server

type ChatServer interface {
	Listen(address string) error
	//TODO maybe we dont need this to be here :?
	//Broadcast(command interface{}, roomName string) error
	Start()
	Close()
}
