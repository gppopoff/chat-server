// Package tui provides the UI for the client console
package tui

import (
	"chat-server/components/client"
	"github.com/marcusolsson/tui-go"
	"strings"
)

func StartUi(c client.ChatClient) {
	loginView := NewLoginView()
	chatView := NewChatView()

	ui, err := tui.New(loginView)
	if err != nil {
		panic(err)
	}

	quit := func() { ui.Quit() }

	ui.SetKeybinding("Esc", quit)
	ui.SetKeybinding("Ctrl+c", quit)
	ui.SetKeybinding("Up", func() { chatView.HistoryScroll.Scroll(0, -1) })
	ui.SetKeybinding("Down", func() { chatView.HistoryScroll.Scroll(0, 1) })
	ui.SetKeybinding("Left", func() { chatView.HistoryScroll.ScrollToTop() })
	ui.SetKeybinding("Right", func() { chatView.HistoryScroll.ScrollToBottom() })

	loginView.OnLogin(func(username string) {
		c.SetName(username)
		ui.SetWidget(chatView)
	})

	// here we define how to handle different submits
	chatView.OnSubmit(func(msg string) {
		if strings.HasPrefix(msg, "/") {
			if strings.HasPrefix(msg, "/join") {
				roomName := strings.Replace(msg, "/join ", "", 1)
				c.JoinRoom(roomName)
				chatView.AddMessage("--- Joining ", roomName+" ---")
			} else if strings.HasPrefix(msg, "/create") {
				roomName := strings.Replace(msg, "/create ", "", 1)
				c.CreateRoom(roomName)
				chatView.AddMessage("--- You created ", roomName+" ---")
			} else if strings.HasPrefix(msg, "/w") {
				msg = strings.Replace(msg, "/w ", "", 1)
				seg := strings.SplitN(msg, " ", 2)
				c.WhisperMessage(seg[0], seg[1])
				chatView.AddMessage("[(To:"+seg[0]+")", seg[1]+"]")
			} else {
				chatView.AddMessage("Invalid command", msg)
			}
		} else {
			c.SendMessage(msg)
		}
	})

	// goroutine that will handle incoming messages
	go func() {
		for msg := range c.Incoming() {
			// we need to make the change via ui update to make sure the ui is repaint correctly
			ui.Update(func() {
				chatView.AddMessage(msg.Name, msg.Message)
			})
		}
	}()

	if err := ui.Run(); err != nil {
		panic(err)
	}
}
