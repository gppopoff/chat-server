package tui

import (
	"fmt"
	tui "github.com/marcusolsson/tui-go"
)

type SubmitMessageHandler func(string)

type ChatView struct {
	tui.Box
	frame         *tui.Box
	history       *tui.Box
	onSubmit      SubmitMessageHandler
	HistoryScroll *tui.ScrollArea
}

// NewChatView: Constructor - creates new chat view - the view when you are in chat mode
func NewChatView() *ChatView {
	view := &ChatView{}

	// ref: https://github.com/marcusolsson/tui-go/blob/master/example/chat/main.go

	view.history = tui.NewVBox()

	view.HistoryScroll = tui.NewScrollArea(view.history)
	//view.HistoryScroll.SetAutoscrollToBottom(true)

	view.HistoryScroll.ScrollToBottom()

	historyBox := tui.NewVBox(view.HistoryScroll)
	historyBox.SetBorder(true)

	input := tui.NewEntry()
	input.SetFocused(true)
	input.SetSizePolicy(tui.Expanding, tui.Maximum)

	input.OnSubmit(func(e *tui.Entry) {
		if e.Text() != "" {
			if view.onSubmit != nil {
				view.onSubmit(e.Text())
			}

			e.SetText("")
		}
	})

	inputBox := tui.NewHBox(input)
	inputBox.SetBorder(true)
	inputBox.SetSizePolicy(tui.Expanding, tui.Maximum)

	view.frame = tui.NewVBox(
		historyBox,
		inputBox,
	)

	view.frame.SetBorder(false)
	view.Append(view.frame)

	return view
}

// OnSubmit: receives handler that determine what to happen when you submit
func (c *ChatView) OnSubmit(handler SubmitMessageHandler) {
	c.onSubmit = handler
}

// AddMessage: Draws new box with the received message and user on the screen
func (c *ChatView) AddMessage(user string, msg string) {
	c.history.Append(
		tui.NewHBox(
			tui.NewLabel(fmt.Sprintf("%v: %v", user, msg)),
		),
	)
	c.HistoryScroll.ScrollToBottom()
}
